import java.util.ArrayList;
import java.util.List;

public class Function {

    public String reverseString(String str){
        String newString = "";
        for (int i = str.length()-1; i >= 0; i--) {
            newString = newString+str.split("")[i];
        }
        return newString;
    }

    public int bagi(int a, int b){
        int m = 0;
        int i = 1;
        while (i <= a){
            if (b * i <= a){
                m = i;
            }
            i++;
        }
        return m;
    }
}
